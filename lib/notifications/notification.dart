import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:remind_me/models/task.dart';
import 'package:remind_me/notifications/receivedNotification.dart';
import 'package:remind_me/notifications/setupNotification.dart';
import 'package:remind_me/screen/tasksScreen/todayTasksScreen.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationFunctions {
  static Future<void> ScheduleNotification(
      int notificationId,
      String notificationBody,
      DateTime reminderDate,
      TimeOfDay reminderTime) async {
    if (reminderDate == null) {
      return;
    }
    await flutterLocalNotificationsPlugin.zonedSchedule(
        notificationId,
        'NEW REMINDER',
        notificationBody,
        _getScheduledTimeDate(reminderDate, reminderTime),
        const NotificationDetails(
            android: AndroidNotificationDetails(
          'your channel id',
          'your channel name',
          'your channel description',
          importance: Importance.max,
          priority: Priority.high,
        )),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.dayOfWeekAndTime);
  }

  static tz.TZDateTime _getScheduledTimeDate(
      DateTime date, TimeOfDay timeOfDay) {
    tz.TZDateTime scheduledDateTime = tz.TZDateTime(tz.local, date.year,
        date.month, date.day, timeOfDay.hour, timeOfDay.minute);
    return scheduledDateTime;
  }

  static Future<void> CancelNotification(int notificationId, Task task) async {
    if (task.reminderDate == null) {
      return;
    }
    await flutterLocalNotificationsPlugin.cancel(notificationId);
  }

  static void RequestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  static void ConfigureDidReceiveLocalNotificationSubject(
      BuildContext context) {
    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification receivedNotification) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: receivedNotification.title != null
              ? Text(receivedNotification.title)
              : null,
          content: receivedNotification.body != null
              ? Text(receivedNotification.body)
              : null,
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
                await Navigator.push(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => TodayTasksScreen()),
                );
              },
              child: const Text('Ok'),
            )
          ],
        ),
      );
    });
  }
}
