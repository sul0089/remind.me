import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/screen/addTaskScreen.dart';
import 'package:remind_me/screen/dashboardScreen.dart';
import 'package:remind_me/screen/landingScreen.dart';
import 'package:remind_me/screen/tasksScreen/allTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/completedTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/overdueTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/todayTasksScreen.dart';
import 'package:remind_me/shared%20prefs/sharedPrefs.dart';
import 'package:remind_me/workManager/workManager.dart';

import 'data/taskProvider.dart';
import 'notifications/notification.dart';
import 'notifications/setupNotification.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SetupNotification.initialNotificationSetup();
  await WorkManager.SetupWorkManager();
  await SharedPrefs.GetTasksFromSharedPrefs();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    NotificationFunctions.ConfigureDidReceiveLocalNotificationSubject(context);
    return ChangeNotifierProvider(
      create: (context) => TaskProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        routes: {
          '/': (context) => LandingScreen(),
          Dashboard.id: (context) => Dashboard(),
          AllTasksScreen.id: (context) => AllTasksScreen(),
          AddUpdateTaskScreen.id: (context) => AddUpdateTaskScreen(),
          CompletedTasksScreen.id: (context) => CompletedTasksScreen(),
          TodayTasksScreen.id: (context) => TodayTasksScreen(),
          OverDueTasksScreen.id: (context) => OverDueTasksScreen(),
        },
      ),
    );
  }
}
