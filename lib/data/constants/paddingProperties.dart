import 'package:flutter/material.dart';

class PaddingProperties {
  static const EdgeInsets K_LIST_ADD_BTN_PADDING =
      EdgeInsets.fromLTRB(30, 10, 30, 30);
  static const EdgeInsets K_TOTAL_TASKS_PADDING =
      EdgeInsets.fromLTRB(20, 15, 20, 0);
  static const EdgeInsets K_LIST_ITEM_PADDING =
      EdgeInsets.fromLTRB(20, 5, 20, 5);
  static const EdgeInsets K_ADD_TASK_SCREEN_PADDING =
      EdgeInsets.fromLTRB(10, 25, 20, 10);

  static const EdgeInsets K_ADD_TASK_SCREEN_BETWEEN_PADDING =
      EdgeInsets.fromLTRB(0, 0, 0, 10);
}
