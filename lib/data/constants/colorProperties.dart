import 'dart:ui';

import 'package:flutter/material.dart';

class ColorProperties {
  static const Color K_BACKGROUND_COLOUR = Color(0xFFEBEDFF);
  static const Color K_APP_BAR_COLOUR = Color(0xFF352855);
  static const Color K_ACTIVE_BTN_COLOUR = Color(0xFF352855);
  static const Color K_INACTIVE_BTN_COLOUR = Colors.grey;
  static const Color K_LIST_TILE_BG_COLOUR = Colors.white;
  static const Color K_ADDTASKSCREEN_ICON_COLOR = Colors.deepPurpleAccent;
}
