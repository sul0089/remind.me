import 'package:flutter/material.dart';

class Constants {
  static const double K_LIST_TILE_CIRCLE_BORDER = 15;
  static const double K_LIST_ITEM_HEIGHT = 80;
  static TimeOfDay defaultReminderTime =
      TimeOfDay.fromDateTime(DateTime(1969, 1, 1, DateTime.now().hour + 1, 00));
  static DateTime defaultReminderDate = DateTime.now();
  static double IconSizeOnAddTask = 30;
  static const String TasksListsSharedPrefKey = "TASKS";
}

enum TaskListType { all, allDue, overdue, duetoday, completed }
