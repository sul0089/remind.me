import 'package:flutter/material.dart';
import 'package:remind_me/models/task.dart';
import 'package:remind_me/notifications/notification.dart';
import 'package:remind_me/shared%20prefs/sharedPrefs.dart';
import 'package:remind_me/utils/helpers.dart';

import 'constants/constants.dart';

class TaskProvider extends ChangeNotifier {
  Task _task;
  int _updateTaskIndex;
  bool taskUpdateFlag = false;
  List<Task> _allTasks = SharedPrefs.ListOfTasks;
  List<Task> _allDueTasks = new List();
  List<Task> _completedTasks = new List();
  List<Task> _overDueTasks = new List();
  List<Task> _dueTodayTasks = new List();
  List<Task> currentList = new List();

  List<Task> _ = new List();
  TaskListType _listType = TaskListType.all;

  set setListType(TaskListType listType) {
    _listType = listType;
    populateListTypes();
    switch (_listType) {
      case TaskListType.all:
        currentList = _allTasks;
        break;
      case TaskListType.allDue:
        currentList = _getAllDueTasks(_allTasks);
        break;
      case TaskListType.completed:
        currentList = _completedTasks;
        break;
      case TaskListType.overdue:
        currentList = _overDueTasks;
        break;
      case TaskListType.duetoday:
        currentList = _dueTodayTasks;
        break;
    }
  }

  void createTask() {
    _task = new Task();
  }

  updateCurrentTask(Task task) {
    _task = task;
    _updateTaskIndex = _allTasks.indexOf(_task);
    taskUpdateFlag = true;
  }

  void deleteCompletedTasks() {
    _completedTasks = new List();
    notifyListeners();
    _addTasksInSharedPrefs();
  }

  void markOverDueTasksAsDone() {
    for (int i = 0; i < currentList.length; i++) {
      updateTickBox(i);
    }
  }

  int get getNumberOfTasks {
    return currentList.length;
  }

  Task getAtTask(int index) {
    populateListTypes();
    return currentList[index];
  }

  void addTask() async {
    _allTasks.add(_task);
    int reminderId = _allTasks.indexOf(_task);
    NotificationFunctions.ScheduleNotification(
        reminderId, _task.title, _task.reminderDate, _task.reminderTime);
    populateListTypes();
    _addTasksInSharedPrefs();
    notifyListeners();
  }

  void updateTask() async {
    NotificationFunctions.CancelNotification(_updateTaskIndex, _task);
    _allTasks.removeAt(_updateTaskIndex);
    _allTasks.insert(_updateTaskIndex, _task);
    NotificationFunctions.ScheduleNotification(
        _updateTaskIndex, _task.title, _task.reminderDate, _task.reminderTime);
    populateListTypes();
    _addTasksInSharedPrefs();
    taskUpdateFlag = false;
    notifyListeners();
  }

  void updateTickBox(int index) {
    Task task = currentList[index];
    task.markAsDone();
    populateListTypes();
    NotificationFunctions.CancelNotification(index, task);
    _addTasksInSharedPrefs();
    notifyListeners();
  }

  void removeTask(int index) {
    Task task = _allTasks[index];
    _allTasks.remove(task);
    populateListTypes();
    NotificationFunctions.CancelNotification(index, task);
    _addTasksInSharedPrefs();
    notifyListeners();
  }

  void updateReminderDate(DateTime date) {
    _task.updateReminderDate(date);
    notifyListeners();
  }

  void updateReminderTime(TimeOfDay timeOfDay) {
    _task.updateReminderTime(timeOfDay);
    notifyListeners();
  }

  DateTime get getReminderDate {
    var date = _task.getReminderDate;
    return date as DateTime;
  }

  TimeOfDay get getReminderTime {
    return _task.getReminderTime;
  }

  void updateTaskTitle(String title) {
    _task.updateTaskTitle(title);
    notifyListeners();
  }

  String get getTaskTitle {
    return _task.getTaskTitle;
  }

  void populateListTypes() {
    _allDueTasks = _getAllDueTasks(_allTasks);
    _completedTasks = _getCompletedTasks(_allTasks);
    _overDueTasks = _getOverDueTasks(_allTasks);
    _dueTodayTasks = _getTodayDueTasks(_allTasks);
  }

  getCompletedTasksCount() {
    return _getCompletedTasks(_completedTasks).length;
  }

  _getCompletedTasks(List<Task> taskList) {
    List<Task> completedTasks = new List();
    taskList.forEach((task) {
      if (task.isDone) {
        completedTasks.add(task);
      }
    });
    return completedTasks;
  }

  getOverDueTasksCount() {
    return _getOverDueTasks(_allDueTasks).length;
  }

  getAllDueTasksCount() {
    return _getAllDueTasks(_allDueTasks).length;
  }

  getTodayDueTasksCount() {
    return _getTodayDueTasks(_allDueTasks).length;
  }

  _getAllDueTasks(List<Task> taskList) {
    List<Task> dueTasks = new List();
    taskList.forEach((task) {
      if (!task.isDone) {
        dueTasks.add(task);
      }
    });
    return dueTasks;
  }

  _getTodayDueTasks(List<Task> taskList) {
    List<Task> dueTodayTasks = new List();
    taskList.forEach((task) {
      if (!task.isDone) {
        if (task.reminderDate != null) {
          if (Helpers.isDateToday(task.reminderDate)) {
            dueTodayTasks.add(task);
          }
        }
      }
    });
    return dueTodayTasks;
  }

  _getOverDueTasks(List<Task> taskList) {
    List<Task> overdueTasks = new List();
    taskList.forEach((task) {
      if (Helpers.IsTaskOverDue(task)) {
        overdueTasks.add(task);
      }
    });
    return overdueTasks;
  }

  _addTasksInSharedPrefs() {
    _allTasks = _completedTasks + _allDueTasks;
    SharedPrefs.AddTaskInSharedPref(_allTasks);
  }
}
