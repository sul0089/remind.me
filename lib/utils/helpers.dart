import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/models/task.dart';

class Helpers {
  static String formatDisplayDate(DateTime date) {
    String formattedDate = "";
    if (date != null) {
      formattedDate = DateFormat.yMMMd().format(date);
    }
    return formattedDate;
  }

  static String formatDisplayTime(TimeOfDay time) {
    String formattedTime = "";
    if (time != null) {
      formattedTime =
          time.toString().replaceAll("TimeOfDay(", "").replaceAll(")", "");
    }
    return formattedTime;
  }

  static bool shouldAddTask(TaskProvider task) {
    bool addTask = true;

    if (task.getReminderDate != null) {
      if (isDateToday(task.getReminderDate)) {
        if (isTimeOfTheDayInPast(task.getReminderTime)) {
          addTask = false;
        }
      }
    }

    if (task.getTaskTitle == null) {
      addTask = false;
    }
    return addTask;
  }

  static DateTime convertTimeOfDayInDateTime(TimeOfDay t) {
    final now = new DateTime.now();
    return new DateTime(now.year, now.month, now.day, t.hour, t.minute);
  }

  static TimeOfDay convertDateTimeTimeOfDay(DateTime dateTime) {
    return TimeOfDay.fromDateTime(dateTime);
  }

  static bool isTimeOfTheDayInPast(TimeOfDay tDay) {
    final now = new DateTime.now();
    DateTime tOfDayWithDate =
        new DateTime(now.year, now.month, now.day, tDay.hour, tDay.minute);
    int hoursDifference = (tOfDayWithDate.hour - now.hour) * 60;
    int minsDifference = tOfDayWithDate.minute - now.minute;
    int totalDifference = hoursDifference + minsDifference;
    return totalDifference < 0;
  }

  static bool isDateToday(DateTime date) {
    final now = new DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final inputDate = DateTime(date.year, date.month, date.day);
    bool dateToday = (inputDate == today);
    return dateToday;
  }

  static bool isDateInPast(DateTime date) {
    final now = new DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final inputDate = DateTime(date.year, date.month, date.day);
    bool datePast = (inputDate.isBefore(today));
    return datePast;
  }

  static bool IsTaskOverDue(Task task) {
    bool isOverdue = false;
    if (!task.isDone) {
      if (task.reminderDate != null) {
        if (_checkDateIsInPast(task)) {
          isOverdue = true;
        }
      }
    }
    return isOverdue;
  }

  static bool _checkDateIsInPast(Task task) {
    if (Helpers.isDateInPast(task.reminderDate)) {
      return true;
    } else if (Helpers.isDateToday(task.reminderDate)) {
      if (Helpers.isTimeOfTheDayInPast(task.reminderTime)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
