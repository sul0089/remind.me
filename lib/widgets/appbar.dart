import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/taskProvider.dart';

class AppBarWidget extends StatelessWidget {
  String headerText;
  bool backLink;
  TaskProvider _taskProvider;
  AppBarWidget({this.headerText, this.backLink = true});
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    return Container(
      color: Colors.transparent,
      child: Container(
        decoration: new BoxDecoration(
          color: ColorProperties.K_APP_BAR_COLOUR,
          borderRadius: new BorderRadius.only(
            bottomLeft: const Radius.circular(25.0),
            bottomRight: const Radius.circular(25.0),
          ),
        ),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 35.0),
                    child: backLinkButton(backLink, context),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    headerText,
                    style: GoogleFonts.breeSerif(
                      textStyle: TextStyle(
                          height: 3, fontSize: 40, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget backLinkButton(bool isBackLinkRequired, BuildContext context) {
    Color linkColor = isBackLinkRequired ? Colors.white : Colors.transparent;
    Function onClickFunction(bool isBckLink) {
      Function onClickCallBack;
      if (isBckLink) {
        onClickCallBack = () {
          Navigator.pop(context);
          _taskProvider.taskUpdateFlag = false;
        };
      } else {
        onClickCallBack = null;
      }
      return onClickCallBack;
    }

    return IconButton(
      icon: new Icon(
        Icons.arrow_back_ios_rounded,
        color: linkColor,
      ),
      onPressed: onClickFunction(isBackLinkRequired),
    );
  }
}
