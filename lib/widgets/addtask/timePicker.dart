import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:interval_time_picker/interval_time_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/utils/helpers.dart';

class TimePicker extends StatelessWidget {
  /// Which holds the selected date
  /// Defaults to today's date.
  DateTime initialTime = DateTime(1969, 1, 1, DateTime.now().hour + 1, 00);
  TimeOfDay selctedTimeOfDay;

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskProvider>(
      builder: (context, taskProvider, child) => Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => _selectTime(context), // Refer step 3
            child: Icon(
              Icons.access_alarm,
              size: Constants.IconSizeOnAddTask,
              color: ColorProperties.K_ADDTASKSCREEN_ICON_COLOR,
            ),
          ),
          Text(
            "${Helpers.formatDisplayTime(Provider.of<TaskProvider>(context, listen: false).getReminderTime)}",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  buildCupertinoTimePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.time,
              initialDateTime: initialTime,
              onDateTimeChanged: (DateTime newDateTime) {
                selctedTimeOfDay =
                    Helpers.convertDateTimeTimeOfDay(newDateTime);
                _updateReminderTime(context);
              },
              use24hFormat: true,
              minuteInterval: 10,
            ),
          );
        });
  }

  buildMaterialTimePicker(BuildContext context) async {
    String _hour, _minute, _time;
    TextEditingController _timeController = TextEditingController();
    final TimeOfDay picked = await showIntervalTimePicker(
      context: context,
      initialTime: selctedTimeOfDay,
      interval: 10,
      visibleStep: VisibleStep.Tenths,
    );
    if (picked != null) selctedTimeOfDay = picked;
    _hour = selctedTimeOfDay.hour.toString();
    _minute = selctedTimeOfDay.minute.toString();
    _time = _hour + ' : ' + _minute;
    _timeController.text = _time;
    _timeController.text = formatDate(
        DateTime(2019, 08, 1, selctedTimeOfDay.hour, selctedTimeOfDay.minute),
        [hh, ':', nn, " ", am]).toString();
    _updateReminderTime(context);
  }

  _selectTime(BuildContext context) {
    _updateReminderTime(context);
    Intl.defaultLocale = 'en_GB';
    final ThemeData theme = Theme.of(context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return buildMaterialTimePicker(context);
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
        return buildCupertinoTimePicker(context);
    }
  }

  void _updateReminderTime(BuildContext context) {
    if (selctedTimeOfDay == null) {
      selctedTimeOfDay = TimeOfDay.fromDateTime(initialTime);
    }
    Provider.of<TaskProvider>(context, listen: false)
        .updateReminderTime(selctedTimeOfDay);

    if (Provider.of<TaskProvider>(context, listen: false).getReminderDate ==
        null) {
      Provider.of<TaskProvider>(context, listen: false)
          .updateReminderDate(Constants.defaultReminderDate);
    }
  }
}
