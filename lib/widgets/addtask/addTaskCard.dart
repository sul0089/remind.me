import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/buttons/updateTaskButton.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/addtask/timePicker.dart';

import '../../buttons/addTaskButton.dart';
import 'datePicker.dart';

class AddUpdateTaskCard extends StatefulWidget {
  @override
  _AddUpdateTaskCardState createState() => _AddUpdateTaskCardState();
}

class _AddUpdateTaskCardState extends State<AddUpdateTaskCard> {
  TaskProvider _taskProvider;
  String errorMessage = "";

  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    return Container(
      color: Colors.white,
      child: Container(
        decoration: new BoxDecoration(
          color: ColorProperties.K_BACKGROUND_COLOUR,
          borderRadius: new BorderRadius.only(
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            DatePicker(),
            TimePicker(),
            _taskProvider.taskUpdateFlag == false
                ? AddTaskBtn(
                    onPressedCallBack: () =>
                        {_taskProvider.addTask(), Navigator.pop(context)},
                  )
                : UpdateTaskBtn(
                    onPressedCallBack: () =>
                        {_taskProvider.updateTask(), Navigator.pop(context)},
                  ),
            Container(
              color: ColorProperties.K_BACKGROUND_COLOUR,
              child: Text(
                errorMessage,
                style: new TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
