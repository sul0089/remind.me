import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/taskProvider.dart';

class InputTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final task = Provider.of<TaskProvider>(context);
    return TextFormField(
      inputFormatters: [
        new LengthLimitingTextInputFormatter(40),
      ],
      initialValue: task.taskUpdateFlag ? task.getTaskTitle : "",
      cursorColor: Colors.black,
      style: TextStyle(
        fontSize: 20,
      ),
      onChanged: (text) {
        task.updateTaskTitle(text);
      },
      decoration: new InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderRadius: new BorderRadius.circular(10.0),
          borderSide:
              new BorderSide(color: ColorProperties.K_ADDTASKSCREEN_ICON_COLOR),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
          borderSide: BorderSide(color: ColorProperties.K_BACKGROUND_COLOUR),
        ),
        hintText: 'Title',
        hintStyle: TextStyle(fontSize: 20.0),
      ),
    );
  }
}
