import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/utils/helpers.dart';

class DatePicker extends StatelessWidget {
  bool updateTask;
  DateTime selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskProvider>(
      builder: (context, taskProvider, child) => Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => _selectDate(context), // Refer step 3
            child: Icon(
              Icons.calendar_today,
              size: Constants.IconSizeOnAddTask,
              color: ColorProperties.K_ADDTASKSCREEN_ICON_COLOR,
            ),
          ),
          Text(
            "${Helpers.formatDisplayDate(Provider.of<TaskProvider>(context, listen: false).getReminderDate)}",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  /// This decides which day will be enabled
  /// This will be called every time while displaying day in calender.
  bool _decideWhichDayToEnable(DateTime day) {
    if ((day.isAfter(DateTime.now().subtract(Duration(days: 1))))) {
      return true;
    }
    return false;
  }

  _selectDate(BuildContext context) {
    updateDate(context);
    Intl.defaultLocale = 'en_GB';
    final ThemeData theme = Theme.of(context);
    assert(theme.platform != null);
    switch (theme.platform) {
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return buildMaterialDatePicker(context);
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
        return buildCupertinoDatePicker(context);
    }
  }

  void updateDate(BuildContext context) {
    Provider.of<TaskProvider>(context, listen: false)
        .updateReminderDate(selectedDate);
    if (Provider.of<TaskProvider>(context, listen: false).getReminderTime ==
        null) {
      Provider.of<TaskProvider>(context, listen: false)
          .updateReminderTime(Constants.defaultReminderTime);
    }
  }

  buildCupertinoDatePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height / 3,
            color: Colors.white70,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (picked) {
                if (picked != null && picked != selectedDate)
                  selectedDate = picked;
                updateDate(context);
              },
              maximumYear: DateTime.now().year + 10,
              minimumDate: DateTime.now().subtract(Duration(days: 0)),
            ),
          );
        });
  }

  buildMaterialDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime.now().subtract(Duration(days: 0)),
      lastDate: DateTime(DateTime.now().year + 10),
      initialEntryMode: DatePickerEntryMode.calendar,
      initialDatePickerMode: DatePickerMode.day,
      selectableDayPredicate: _decideWhichDayToEnable,
      helpText: 'Select a reminder date',
      cancelText: 'Cancel',
      confirmText: 'Select',
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'You can not select a date in the past',
      fieldLabelText: 'Reminder date',
      fieldHintText: 'Date/Month/Year',
      builder: (context, child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate) selectedDate = picked;
    updateDate(context);
  }
}
