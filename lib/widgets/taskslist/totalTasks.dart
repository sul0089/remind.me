import 'package:flutter/material.dart';

class TotalTasksText extends StatelessWidget {
  int numberOfTasks;
  TotalTasksText({this.numberOfTasks});
  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: new TextSpan(
        style: new TextStyle(
          fontSize: 17.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(
            text: 'YOU HAVE ',
            style: new TextStyle(fontSize: 20),
          ),
          new TextSpan(
            text: '${numberOfTasks == 0 ? 'NO' : numberOfTasks}',
            style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          new TextSpan(
            text:
                '${(numberOfTasks > 1 || numberOfTasks == 0) ? ' TASKS' : ' TASK'}  ',
            style: new TextStyle(fontSize: 20),
          ),
        ],
      ),
    );
  }
}
