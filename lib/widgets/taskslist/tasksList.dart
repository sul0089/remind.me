import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/models/task.dart';

import 'list_item.dart';

class TasksList extends StatelessWidget {
  String listType;
  TasksList({this.listType});
  @override
  Widget build(BuildContext context) {
    return Consumer<TaskProvider>(
      builder: (context, taskData, child) {
        return ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final Task task =
                Provider.of<TaskProvider>(context).getAtTask(index);
            return Dismissible(
              dismissThresholds: {
                DismissDirection.startToEnd: 0.7,
                DismissDirection.endToStart: 0.7
              },
              movementDuration: Duration(seconds: 1),
              background: slideRightBackground(MainAxisAlignment.start),
              secondaryBackground: slideRightBackground(MainAxisAlignment.end),
              key: Key(task.title),
              confirmDismiss: (DismissDirection direction) async {
                return await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialogWidget(index: index);
                  },
                );
              },
              child: ListItem(
                task: task,
                isDoneCallBack: (bool checkBoxState) {
                  Provider.of<TaskProvider>(context, listen: false)
                      .updateTickBox(index);
                },
              ),
            );
          },
          itemCount: Provider.of<TaskProvider>(context).getNumberOfTasks,
        );
      },
    );
  }
}

Widget slideRightBackground(MainAxisAlignment mainAxisAlignment) {
  return Container(
    color: Colors.red,
    child: Align(
      child: Row(
        mainAxisAlignment: mainAxisAlignment,
        children: <Widget>[
          Icon(
            Icons.delete,
            color: Colors.white,
          ),
          Text(
            " Delete",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
            textAlign: TextAlign.right,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      alignment: Alignment.centerRight,
    ),
  );
}

class AlertDialogWidget extends StatelessWidget {
  int index;
  AlertDialogWidget({this.index});

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskProvider>(
      builder: (context, taskProvider, child) => AlertDialog(
        title: const Text("Confirm"),
        content: const Text("Are you sure you wish to delete this item?"),
        actions: <Widget>[
          FlatButton(
              onPressed: () => {
                    Provider.of<TaskProvider>(context, listen: false)
                        .removeTask(index),
                    Navigator.of(context).pop(true)
                  },
              child: const Text("DELETE")),
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: const Text("CANCEL"),
          ),
        ],
      ),
    );
  }
}
