import 'package:flutter/cupertino.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/taskslist/tasksList.dart';
import 'package:remind_me/widgets/taskslist/totalTasks.dart';

import '../appbar.dart';

class TaskListWidget extends StatelessWidget {
  TaskProvider taskProvider;
  String appBarTitle;
  Widget buttonWidget;

  TaskListWidget(this.taskProvider, this.appBarTitle, this.buttonWidget);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        AppBarWidget(headerText: appBarTitle),
        Padding(
          padding: PaddingProperties.K_TOTAL_TASKS_PADDING,
          child: TotalTasksText(numberOfTasks: taskProvider.getNumberOfTasks),
        ),
        Expanded(child: TasksList()),
        buttonWidget,
      ],
    );
  }
}
