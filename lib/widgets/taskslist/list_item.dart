import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/models/task.dart';
import 'package:remind_me/screen/addTaskScreen.dart';
import 'package:remind_me/utils/helpers.dart';

class ListItem extends StatelessWidget {
  final Function isDoneCallBack;
  final Task task;
  TaskProvider _taskProvider;

  ListItem({
    this.task,
    this.isDoneCallBack,
  });
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);

    return Padding(
      padding: PaddingProperties.K_LIST_ITEM_PADDING,
      child: Container(
        color: ColorProperties.K_BACKGROUND_COLOUR,
        child: Container(
          decoration: new BoxDecoration(
            color: ColorProperties.K_LIST_TILE_BG_COLOUR,
            borderRadius: new BorderRadius.only(
              bottomLeft: Radius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
              bottomRight: Radius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
              topLeft: Radius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
              topRight: Radius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
            ),
          ),
          height: Constants.K_LIST_ITEM_HEIGHT,
          child: ListTile(
            title: Text(
              task.title,
              style: (TextStyle(
                  fontSize: 20,
                  color:
                      Helpers.IsTaskOverDue(task) ? Colors.red : Colors.black)),
            ),
            subtitle: InkWell(
              child: Text(
                getDisplayDate() + "   " + getDisplayTime(),
                style: (TextStyle(
                  fontSize: 15,
                )),
              ),
              onTap: () {
                _taskProvider.updateCurrentTask(task);
                Navigator.pushNamed(context, AddUpdateTaskScreen.id);
              },
            ),
            trailing: Checkbox(
                activeColor: ColorProperties.K_APP_BAR_COLOUR,
                value: task.isDone,
                onChanged: isDoneCallBack),
          ),
        ),
      ),
    );
  }

  String getDisplayDate() {
    String displayDate = "";
    if (task.reminderDate != null) {
      if (Helpers.isDateToday(task.reminderDate)) {
        displayDate = 'TODAY';
      } else {
        displayDate = Helpers.formatDisplayDate(task.reminderDate);
      }
    }
    return displayDate;
  }

  String getDisplayTime() {
    String displayTime = "";
    if (task.reminderTime != null) {
      displayTime = Helpers.formatDisplayTime(task.reminderTime);
    }
    return displayTime;
  }
}
