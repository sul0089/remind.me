import 'package:flutter/material.dart';
import 'package:remind_me/data/constants/colorProperties.dart';

class DashboardTile extends StatelessWidget {
  String text;
  Icon icon;
  String numberOfTasks;
  String navigationRoute;
  DashboardTile(
      {this.text, this.icon, this.numberOfTasks, this.navigationRoute});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(3.0, 5.0, 3.0, 0.0),
      child: Container(
        color: Colors.white,
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, navigationRoute);
          },
          child: Container(
            decoration: new BoxDecoration(
              color: ColorProperties.K_APP_BAR_COLOUR,
              borderRadius: new BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: icon,
                      ),
                      Text(
                        text,
                        style: TextStyle(
                            color: Colors.white70,
                            fontSize: 15,
                            fontWeight: FontWeight.w900),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          numberOfTasks,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
