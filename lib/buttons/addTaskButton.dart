import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/utils/helpers.dart';

class AddTaskBtn extends StatelessWidget {
  Function onPressedCallBack;

  AddTaskBtn({this.onPressedCallBack});
  @override
  Widget build(BuildContext context) {
    final task = Provider.of<TaskProvider>(context, listen: false);
    return Consumer<TaskProvider>(
      builder: (context, taskProvider, child) => Padding(
        padding: PaddingProperties.K_LIST_ADD_BTN_PADDING,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
          ),
          color: Helpers.shouldAddTask(task)
              ? ColorProperties.K_ACTIVE_BTN_COLOUR
              : ColorProperties.K_INACTIVE_BTN_COLOUR,
          textColor: Colors.white,
          onPressed: () => {
            if (Helpers.shouldAddTask(task)) {onPressedCallBack()}
          },
          child: Text(
            "Add new Task".toUpperCase(),
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }
}
