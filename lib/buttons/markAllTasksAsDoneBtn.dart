import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';

class MarkAllTasksAsDoneButton extends StatelessWidget {
  TaskProvider _taskProvider;
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    return Padding(
      padding: PaddingProperties.K_LIST_ADD_BTN_PADDING,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
        ),
        color: _getButtonColour(),
        textColor: Colors.white,
        onPressed: () => {
          if (_taskProvider.getNumberOfTasks != 0) {_onClickCallBack(context)},
        },
        child: Text(
          "Complete All".toUpperCase(),
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }

  Color _getButtonColour() {
    Color color = ColorProperties.K_ACTIVE_BTN_COLOUR;
    if (_taskProvider.getNumberOfTasks == 0) {
      color = ColorProperties.K_INACTIVE_BTN_COLOUR;
    }
    return color;
  }

  _onClickCallBack(context) {
    _taskProvider.markOverDueTasksAsDone();
  }
}
