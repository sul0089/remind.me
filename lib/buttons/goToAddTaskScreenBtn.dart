import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/screen/addTaskScreen.dart';

class GoToAddTask extends StatelessWidget {
  TaskProvider _taskProvider;
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    return Padding(
      padding: PaddingProperties.K_LIST_ADD_BTN_PADDING,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(Constants.K_LIST_TILE_CIRCLE_BORDER),
        ),
        color: ColorProperties.K_ACTIVE_BTN_COLOUR,
        textColor: Colors.white,
        onPressed: () => {_onClickCallBack(context)},
        child: Text(
          "Add new Task".toUpperCase(),
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }

  _onClickCallBack(context) {
    _taskProvider.setListType = TaskListType.all;
    _taskProvider.createTask();
    Navigator.pushNamed(context, AddUpdateTaskScreen.id);
  }
}
