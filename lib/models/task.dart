import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:remind_me/utils/helpers.dart';

class Task {
  String title;
  bool isDone;
  TimeOfDay reminderTime;
  DateTime reminderDate;

  Task({this.title, this.isDone = false, this.reminderDate, this.reminderTime});

  void markAsDone() {
    isDone = !isDone;
  }

  bool get taskDoneStatus {
    return isDone;
  }

  void updateReminderTime(TimeOfDay tOfDay) {
    reminderTime = tOfDay;
  }

  TimeOfDay get getReminderTime {
    return reminderTime;
  }

  void updateReminderDate(DateTime remindDate) {
    reminderDate = remindDate;
  }

  get getReminderDate {
    return reminderDate;
  }

  void updateTaskTitle(String taskTitle) {
    title = taskTitle;
  }

  String get getTaskTitle {
    return title;
  }

  factory Task.fromJson(Map<String, dynamic> jsonData) {
    return Task(
      title: jsonData['title'],
      reminderDate: _decodeReminderDate(jsonData['reminderDate']),
      reminderTime: _decodeReminderTime(jsonData['reminderTime']),
      isDone: jsonData['isDone'],
    );
  }

  static Map<String, dynamic> toMap(Task task) => {
        'title': task.title,
        'reminderDate': _encodeReminderDate(task.reminderDate),
        'reminderTime': _encodeReminderTime(task.reminderTime),
        'isDone': task.isDone,
      };

  static String encode(List<Task> tasks) => json.encode(
        tasks.map<Map<String, dynamic>>((task) => Task.toMap(task)).toList(),
      );

  static List<Task> decode(String tasks) =>
      (json.decode(tasks) as List<dynamic>)
          .map<Task>((item) => Task.fromJson(item))
          .toList();

  static String _encodeReminderDate(DateTime date) {
    String encodedDate = "";
    if (date != null) {
      encodedDate = date.toIso8601String();
    }
    return encodedDate;
  }

  static String _encodeReminderTime(TimeOfDay timeofDay) {
    String timeOfDay = "";
    if (timeofDay != null) {
      timeOfDay =
          Helpers.convertTimeOfDayInDateTime(timeofDay).toIso8601String();
    }
    return timeOfDay;
  }

  static DateTime _decodeReminderDate(String dateTime) {
    DateTime decodedDate = null;
    if (dateTime != "") {
      decodedDate = DateTime.parse(dateTime);
    }
    return decodedDate;
  }

  static TimeOfDay _decodeReminderTime(String timeOfDay) {
    TimeOfDay decodeTimeofDay = null;
    if (timeOfDay != "") {
      decodeTimeofDay =
          Helpers.convertDateTimeTimeOfDay(DateTime.parse(timeOfDay));
    }

    return decodeTimeofDay;
  }
}
