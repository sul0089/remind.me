import 'dart:io';

import 'package:workmanager/workmanager.dart';

// setup app as background process
class WorkManager {
  static SetupWorkManager() async {
    await Workmanager.initialize(
        _callbackDispatcher, // The top level function, aka callbackDispatcher
        isInDebugMode:
            false // If enabled it will post a notification whenever the task is running. Handy for debugging tasks
        );

    if (Platform.isAndroid) {
      await Workmanager.registerPeriodicTask(
        "tasks_notification_21211",
        "taskName",
        inputData: {"data": "vale"},
      );
    }
  }

  static void _callbackDispatcher() {
    Workmanager.executeTask(
      (task, inputData) {
        print(
            "Native called background task: $task"); //simpleTask will be emitted here.
        return Future.value(true);
      },
    );
  }
}
