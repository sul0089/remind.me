import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/buttons/goToAddTaskScreenBtn.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/taskslist/tasksListWidget.dart';

class AllTasksScreen extends StatelessWidget {
  static const String id = 'all_tasks_screen';
  TaskProvider _taskProvider;

  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    _taskProvider.setListType = TaskListType.allDue;

    return Scaffold(
        backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
        body: TaskListWidget(_taskProvider, 'ALL', GoToAddTask()));
  }
}
