import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/buttons/goToAddTaskScreenBtn.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/taskslist/tasksListWidget.dart';

class TodayTasksScreen extends StatelessWidget {
  static const String id = 'today_tasks_screen';
  TaskProvider _taskProvider;
  String payload;
  TodayTasksScreen({this.payload});

  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    _taskProvider.setListType = TaskListType.duetoday;

    return Scaffold(
        backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
        body: TaskListWidget(_taskProvider, 'TODAY', GoToAddTask()));
  }
}
