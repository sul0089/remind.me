import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/buttons/markAllTasksAsDoneBtn.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/taskslist/tasksListWidget.dart';

class OverDueTasksScreen extends StatelessWidget {
  static const String id = 'tasks_screen';
  TaskProvider _taskProvider;

  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    _taskProvider.setListType = TaskListType.overdue;

    return Scaffold(
        backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
        body: TaskListWidget(
            _taskProvider, 'OVERDUE', MarkAllTasksAsDoneButton()));
  }
}
