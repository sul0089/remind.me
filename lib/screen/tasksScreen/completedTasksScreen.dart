import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/buttons/deleteAllCompletedTasksBtn.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/widgets/taskslist/tasksListWidget.dart';

class CompletedTasksScreen extends StatelessWidget {
  static const String id = 'completed_tasks_screen';
  TaskProvider _taskProvider;

  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    _taskProvider.setListType = TaskListType.completed;

    return Scaffold(
      backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
      body: TaskListWidget(
          _taskProvider, 'COMPLETED', DeleteAllCompletedTasksButton()),
    );
  }
}
