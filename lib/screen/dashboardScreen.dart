import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/notifications/notification.dart';
import 'package:remind_me/screen/tasksScreen/allTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/completedTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/overdueTasksScreen.dart';
import 'package:remind_me/screen/tasksScreen/todayTasksScreen.dart';
import 'package:remind_me/widgets/appbar.dart';
import 'package:remind_me/widgets/dashboard/dashboardTile.dart';

class Dashboard extends StatelessWidget {
  TaskProvider _taskProvider;
  static const String id = 'dashboard_screen';
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    _taskProvider.populateListTypes();
    NotificationFunctions.RequestPermissions();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: AppBarWidget(
              headerText: 'DASHBOARD',
              backLink: false,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: DashboardTile(
                          text: 'ALL',
                          numberOfTasks:
                              _taskProvider.getAllDueTasksCount().toString(),
                          navigationRoute: AllTasksScreen.id,
                          icon: Icon(
                            Icons.create,
                            color: Colors.green[300],
                            size: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: DashboardTile(
                          text: 'TODAY',
                          numberOfTasks:
                              _taskProvider.getTodayDueTasksCount().toString(),
                          navigationRoute: TodayTasksScreen.id,
                          icon: Icon(
                            Icons.list,
                            color: Colors.orange[600],
                            size: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: DashboardTile(
                          text: 'OVERDUE',
                          numberOfTasks:
                              _taskProvider.getOverDueTasksCount().toString(),
                          navigationRoute: OverDueTasksScreen.id,
                          icon: Icon(
                            Icons.calendar_today,
                            color: Colors.red[600],
                            size: 40,
                          ),
                        ),
                      ),
                      Expanded(
                        child: DashboardTile(
                          text: 'COMPLETED',
                          numberOfTasks:
                              _taskProvider.getCompletedTasksCount().toString(),
                          navigationRoute: CompletedTasksScreen.id,
                          icon: Icon(
                            Icons.check_box_rounded,
                            color: Colors.green[300],
                            size: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
