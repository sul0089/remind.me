import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:remind_me/data/constants/colorProperties.dart';
import 'package:remind_me/screen/dashboardScreen.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class LandingScreen extends StatefulWidget {
  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(milliseconds: 2500), () {
      Navigator.pushNamed(context, Dashboard.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorProperties.K_BACKGROUND_COLOUR,
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/checked.png',
                  fit: BoxFit.cover,
                  scale: 9,
                ),
                TypewriterAnimatedTextKit(
                  speed: Duration(milliseconds: 150),
                  isRepeatingAnimation: false,
                  text: [
                    "REMIND.ME",
                  ],
                  textStyle: TextStyle(
                      fontSize: 40.0,
                      fontFamily: "Anton",
                      fontWeight: FontWeight.w900,
                      color: ColorProperties.K_APP_BAR_COLOUR),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 100.0),
              child: SleekCircularSlider(
                appearance: CircularSliderAppearance(
                    size: 100,
                    customWidths: CustomSliderWidths(progressBarWidth: 10)),
                min: 0,
                max: 100,
                initialValue: 100,
              ),
            ),
          ]),
    );
  }
}
