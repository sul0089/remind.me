import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:remind_me/data/constants/paddingProperties.dart';
import 'package:remind_me/data/taskProvider.dart';
import 'package:remind_me/models/task.dart';
import 'package:remind_me/widgets/addtask/addTaskCard.dart';
import 'package:remind_me/widgets/addtask/inputTextfield.dart';
import 'package:remind_me/widgets/appbar.dart';

class AddUpdateTaskScreen extends StatelessWidget {
  static const String id = 'add_task_screen';
  TaskProvider _taskProvider;
  Task task;
  @override
  Widget build(BuildContext context) {
    _taskProvider = Provider.of<TaskProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          AppBarWidget(
              headerText: _taskProvider.taskUpdateFlag == false
                  ? 'NEW TASK'
                  : 'UPDATE TASK'),
          Padding(
            padding: PaddingProperties.K_ADD_TASK_SCREEN_PADDING,
            child: Column(
              children: [
                Padding(
                  padding: PaddingProperties.K_ADD_TASK_SCREEN_BETWEEN_PADDING,
                  child: InputTextField(),
                ),
                AddUpdateTaskCard(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
