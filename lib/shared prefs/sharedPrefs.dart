import 'package:remind_me/data/constants/constants.dart';
import 'package:remind_me/models/task.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static SharedPreferences prefs;
  static List<Task> ListOfTasks = new List();

  static Future<List<Task>> GetTasksFromSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    List<Task> _tasks = new List();
    if (prefs.get(Constants.TasksListsSharedPrefKey) != null) {
      _tasks =
          await SharedPrefs._getListOfTasks(Constants.TasksListsSharedPrefKey);
    }

    ListOfTasks = _tasks;
  }

  static void AddTaskInSharedPref(List<Task> tasks) {
    var tasksString = Task.encode(tasks);
    SharedPrefs._saveListOfTasks(
        Constants.TasksListsSharedPrefKey, tasksString);
  }

  static void _saveListOfTasks(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<List<Task>> _getListOfTasks(String key) async {
    String listOfTasks = prefs.getString(key);
    List<Task> tasks = Task.decode(listOfTasks);
    return tasks;
  }
}
